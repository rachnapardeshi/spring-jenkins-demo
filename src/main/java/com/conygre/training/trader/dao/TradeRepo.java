package com.conygre.training.trader.dao;

import com.conygre.training.trader.model.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepo extends MongoRepository<Trade, ObjectId> {

}