package com.conygre.training.trader.service;

import java.util.Collection;

import com.conygre.training.trader.model.Trade;

import org.springframework.stereotype.Service;

@Service
public interface TradeService {
    void addTrade(Trade trade);

    Collection<Trade> getAllTrades();
}